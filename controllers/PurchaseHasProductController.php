<?php

namespace app\controllers;

use Yii;
use app\models\PurchaseHasProduct;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PurchaseHasProductController implements the CRUD actions for PurchaseHasProduct model.
 */
class PurchaseHasProductController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all PurchaseHasProduct models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => PurchaseHasProduct::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PurchaseHasProduct model.
     * @param integer $purchase_id
     * @param integer $product_id
     * @return mixed
     */
    public function actionView($purchase_id, $product_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($purchase_id, $product_id),
        ]);
    }

    /**
     * Creates a new PurchaseHasProduct model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new PurchaseHasProduct();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'purchase_id' => $model->purchase_id, 'product_id' => $model->product_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing PurchaseHasProduct model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $purchase_id
     * @param integer $product_id
     * @return mixed
     */
    public function actionUpdate($purchase_id, $product_id)
    {
        $model = $this->findModel($purchase_id, $product_id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'purchase_id' => $model->purchase_id, 'product_id' => $model->product_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing PurchaseHasProduct model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $purchase_id
     * @param integer $product_id
     * @return mixed
     */
    public function actionDelete($purchase_id, $product_id)
    {
        $this->findModel($purchase_id, $product_id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the PurchaseHasProduct model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $purchase_id
     * @param integer $product_id
     * @return PurchaseHasProduct the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($purchase_id, $product_id)
    {
        if (($model = PurchaseHasProduct::findOne(['purchase_id' => $purchase_id, 'product_id' => $product_id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
