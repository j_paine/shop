<?php

namespace app\controllers;

use Yii;
use app\models\Product;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\SqlDataProvider;
use yii\helpers\Json;
use yii\db\Connection;
use yii\db\Command;

/**
 * ProductController implements the CRUD actions for Product model.
 */
class ProductController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Product models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Product::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Product model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Product model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Product();
        if (Yii::$app->request->isAjax) {
            return $this->createProduct(Yii::$app->request->post());
        }
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Product model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Product model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    
    public function actionUsers()
    {
        $results = [];
        
        if (($models = Product::find()->users()->all()) !== null) {
            foreach ($models as $model)
            {
                $results[] = ['value' => 
                    $model->brand . ', ' . $model->type . ', ' . $model->desc
                ];
            }
        }
        return json_encode($results);
    }
    
    public function actionFind($q)
    {
        $results = [];
        $db = Yii::$app->db;
        $products = $db->createCommand("SELECT * FROM (" .
                "SELECT id,brand,type,[[desc]],amount,unit,user_id," .
                    "CONCAT_WS(' ',brand,type,[[desc]],amount)" .
                    " AS prod" . 
                " FROM product) t " .
                "WHERE t.prod LIKE :query AND t.user_id=:user")
            ->bindValue(':query', "%$q%")
            ->bindValue(':user', Yii::$app->user->id)
            ->queryAll();
        
        foreach ($products as $product) {
            $results[] = [
                'id' => $product['id'],
                'brand' => $product['brand'],
                'type' => $product['type'],
                'desc' => $product['desc'],
                'amount' => $product['amount'],
                'unit' => $product['unit'],
                'value' =>  $product['brand'] . ', ' .
                            $product['type'] . ', ' .
                            $product['desc'] . 
                            (($product['amount'] !== null) ?
                                ' (' . $product['amount'] . ' ' .
                                $product['unit'] . ')' :
                                '')
                            
            ];
        }
        echo Json::encode($results);
    }
    
    public function actionByBrand($type = null, $desc = null)
    {
        $results = [];
        $products = Product::find()->users()
                ->where($type ? 'type LIKE "%' . $type .'%"' : '')
                ->andWhere($desc ? 'desc LIKE "%' . $desc .'%"' : '')
                ->all();
        foreach ($products as $product) 
        {
            $results[] = ['value' => $product->brand];
        }
        return json_encode($results);
    }
    public function actionByType($brand = null, $desc = null)
    {
        $results = [];
        $products = Product::find()->users()
                ->where($brand ? 'brand LIKE "%' . $brand .'%"' : '')
                ->andWhere($desc ? 'desc LIKE "%' . $desc .'%"' : '')
                ->all();
        foreach ($products as $product) 
        {
            $results[] = ['value' => $product->type];
        }
        return json_encode($results);
    }
    public function actionByDesc($type = null, $brand = null)
    {
        $results = [];
        $products = Product::find()->users()
                ->where($type ? 'type LIKE "%' . $type .'%"' : '')
                ->andWhere($brand ? 'desc LIKE "%' . $brand .'%"' : '')
                ->all();
        foreach ($products as $product) 
        {
            $results[] = ['value' => $product->desc];
        }
        return json_encode($results);
    }

    /**
     * Finds the Product model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Product the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Product::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    private function createProduct($data)
    {
        $product = new Product();
        $attrs = explode(',', $data['name']);
        $product->setAttributes([
            'brand' => $attrs[0],
            'type' => $attrs[1],
            'desc' => $attrs[2],
            'amount' => $attrs[3],
            'unit' => $attrs[4],
        ]);
        
        if ($product->save())
            return $product->id;
        else
            return 0;
    }
}
