<?php

namespace app\controllers;

use Yii;
use app\models\Purchase;
use app\models\PurchaseHasProduct;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PurchaseController implements the CRUD actions for Purchase model.
 */
class PurchaseController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Purchase models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Purchase::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Purchase model.
     * 
     * 
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
            'hasProductModel' => new \app\models\PurchaseHasProduct,
            'productModel' => new \app\models\Product,
        ]);
    }

    /**
     * Creates a new Purchase model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Purchase();
        $hasProduct = new \app\models\PurchaseHasProduct();
        $product = new \app\models\Product();

        if (Yii::$app->request->isAjax) {
            if ($this->createPurchase(Yii::$app->request->post())) {
                Yii::$app->getSession()->setFlash('success', 'Purchase created');
                return true; //$this->redirect(['create']);
            } else {
                return false;
            }
            
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'hasProduct' => $hasProduct,
                'product' => $product,
            ]);
        }

    }

    /**
     * Updates an existing Purchase model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Purchase model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
		
        return $this->redirect(['index']);
    }

    /**
     * Finds the Purchase model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Purchase the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Purchase::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    private function createPurchase($data)
    {
        $purchase = new Purchase();
        $purchase->setAttributes([
            'store_id' => intval($data['purchase']['store']),
            'date' => intval($data['purchase']['date'])
        ]);
        $purchase->save();

        foreach ($data['products'] as $product)
        {
            $hasProduct = new PurchaseHasProduct();
            $hasProduct->setAttributes([
                'purchase_id' => $purchase->id,
                'product_id' => intval($product['productId']),
                'qty' => intval($product['qty']),
                'cost' => $product['cost'],
                'discount' => $product['discount']
            ], false);
            $result = $hasProduct->save();
            if (!$result) break;
        }

        return true;
    }
}
