'use strict';

// # Globbing
// for performance reasons we're only matching one level down:
// 'test/spec/{,*/}*.js'
// If you want to recursively match all subfolders, use:
// 'test/spec/**/*.js'

module.exports = function (grunt) {

  // Time how long tasks take. Can help when optimizing build times
  require('time-grunt')(grunt);

  // Load grunt tasks automatically
  require('load-grunt-tasks')(grunt);

  // Configurable paths
  var config = {
    public: 'web',
    mysqlUser: 'mysql',
    prodDb: 'shop',
    testDb: 'shop_test'
  };

  // Define the configuration for all the tasks
  grunt.initConfig({
    config: config,
    clean: {
      
    },
    watch: {
      php: {
        files: ['**/*.php'],
        options: {
          livereload: true
        }
      },
      js: {
        files: ['**/*.js'],
        options: {
          livereload: true
        }
      },
      css: {
        files: ['**/*.css'],
        options: {
          livereload: true
        }
      }
    },
    php: {
      options: {
        port: 8000,
        open: false,
        livereload: 35729,
        base: '<%= config.public %>',
        keepalive: false
      },
      dev: {}
    },
    open: {
      dev: {
	app: 'google-chrome-unstable'
      },
      mail: {
        app: 'google-chrome-unstable',
        path: '127.0.0.1:1080'
      }
    },
    shell: {
      options: {
        stdout: true
      },
      test: {
        command: 'cd tests; codecept run; cd ..'
      },
      init: {
        command: 'cd tests; codecept build; cd ..'
      },
      prepDb: {
        command: 'mysqldump -h localhost -u <%= config.mysqlUser %> <%= config.prodDb %> | mysql -h localhost -u <%= config.mysqlUser %> <%= config.testDb %>'
      },
      mailcatcher: {
        command: 'pkill -9 ruby; mailcatcher'
      }
    }
  });

  grunt.registerTask('serve', 'start the server and preview your app, --allow-remote for remote access', function (target) {
    if (grunt.option('allow-remote')) {
      grunt.config.set('php.options.hostname', '0.0.0.0');
    } else {
      grunt.config.set('php.options.hostname', '127.0.0.1');
    }
	
    // Set grunt-open path
    grunt.config.set(
      'open.dev.path', '<%= php.options.hostname %>:<%= php.options.port %>'
    );

    grunt.task.run([
      'shell:mailcatcher',
      'open:mail',
      'php:dev',
      'open:dev',
      'watch'
    ]);
  });
  
  grunt.registerTask('test', function(target) {
    grunt.task.run([
      'test:prepDb',
      target ? ('shell:' + target) : 'shell:test'
    ]);
  });
};
