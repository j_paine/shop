<?php

namespace app\assets;

use yii\web\AssetBundle;

class LivereloadAsset extends AssetBundle
{
    public $js = [
        'http://127.0.0.1:35729/livereload.js',
    ];
    
}
