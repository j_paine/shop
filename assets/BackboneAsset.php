<?php
namespace app\assets;

use yii\web\AssetBundle;

class BackboneAsset extends AssetBundle
{
    public $sourcePath = '@bower';
  
    public $js = [
        'underscore/underscore.js',
        'backbone/backbone.js',
        'backbone.localstorage/backbone.localStorage.js',
        'backbone.babysitter/lib/backbone.babysitter.js',
        'backbone.wreqr/lib/backbone.wreqr.js',
        'backbone.marionette/lib/backbone.marionette.js',
        'backbone.syphon/lib/backbone.syphon.js',
    ];
    
    public $depends = [
        'yii\web\JqueryAsset'
    ];
}
