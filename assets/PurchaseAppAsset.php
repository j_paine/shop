<?php

namespace app\assets;

use yii\web\AssetBundle;

class PurchaseAppAsset extends AssetBundle
{
    public $basePath = '@webroot';

    public $js = [
        'js/purchaseApp/app.js',
        'js/purchaseApp/entities.js',
        'js/purchaseApp/apps/purchase_form/view.js',
        'js/purchaseApp/apps/purchase_form/controller.js',
        'js/purchaseApp/apps/product_form/view.js',
        'js/purchaseApp/apps/product_form/controller.js',        
        'js/purchaseApp/apps/products_table/view.js',
        'js/purchaseApp/apps/products_table/controller.js',        
    ];
    
    public $depends = [
        'app\assets\BackboneAsset'
    ];
}
