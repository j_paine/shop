<?php

use yii\db\Schema;
use yii\db\Migration;

class m141101_150247_create_purchase_table extends Migration
{
    public function up()
    {
		$this->createTable('purchase', [
			'id' => 'pk',
			'date' => 'integer NOT NULL',
			'store_id' => 'integer NOT NULL',
			'user_id' => 'integer NOT NULL',
			'created_at' => 'integer NOT NULL',
			'updated_at' => 'integer',
		]);
		
		$this->addForeignKey(
			'fk_purchase_store', 'purchase', 'store_id', 'store', 'id', 'CASCADE', 'CASCADE'
		);
		$this->addForeignKey(
			'fk_purchase_user', 'purchase', 'user_id', 'user', 'id', 'CASCADE', 'CASCADE'
		);
    }

    public function down()
    {
        return $this->dropTable('purchase');
    }
}
