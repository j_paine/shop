<?php

use yii\db\Schema;
use yii\db\Migration;

class m141102_164433_add_purchase_data extends Migration
{
    public function up()
    {
		$this->insert('purchase', [
			'date' => Yii::$app->formatter->asDate('01/01/01','short'),
			'store_id' => 1,
			'user_id' => 1,
		]);
		
		$this->insert('purchase', [
			'date' => Yii::$app->formatter->asDate('01/01/01','short'),
			'store_id' => 2,
			'user_id' => 1,
		]);
		
    }

    public function down()
    {
        $this->delete('purchase', [
			'date' => Yii::$app->formatter->asDate('01/01/01','short'),
			'store_id' => 1,
			'user_id' => 1,
		]);
		
		$this->insert('purchase', [
			'date' => Yii::$app->formatter->asDate('01/01/01','short'),
			'store_id' => 2,
			'user_id' => 1,
		]);
    }
}
