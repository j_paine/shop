<?php

use yii\db\Schema;
use yii\db\Migration;

class m141101_150300_create_purchase_has_product_table extends Migration
{
    public function up()
    {
		$this->createTable('purchase_has_product', [
			'purchase_id' => 'integer NOT NULL',
			'product_id' => 'integer NOT NULL',
			'qty' => 'integer',
			'cost' => 'money NOT NULL',
			'discount' => 'string',
		]);
		
		$this->addPrimaryKey(
			'pk_purchase_has_product', 
			'purchase_has_product', 
			['purchase_id','product_id']
		);
		
		$this->addForeignKey(
			'fk_purchase_has_product_purchase',
			'purchase_has_product',
			'purchase_id',
			'product',
			'id',
			'CASCADE',
			'CASCADE'
		);
		
		$this->addForeignKey(
                        'fk_purchase_has_product_product',
			'purchase_has_product',
			'product_id',
			'product',
			'id',
			'CASCADE',
			'CASCADE'
		);
    }

    public function down()
    {
        return $this->dropTable('purchase_has_product');
    }
}
