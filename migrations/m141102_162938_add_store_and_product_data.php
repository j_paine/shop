<?php

use yii\db\Schema;
use yii\db\Migration;
use dektrium\user\models\User;

class m141102_162938_add_store_and_product_data extends Migration
{
    public function up()
    {
		$this->insert('store', [
			'name' => 'Kvickly',
			'url' => 'http://www.kvickly.dk',
			'offers_url' => 'http://www.kvickly.dk/tilbud',
			'user_id' => 1,
		]);
		
		$this->insert('store', [
			'name' => 'Føtex',
			'url' => 'http://www.foetex.dk',
			'offers_url' => 'http://www.foetex.dk/tilbud',
			'user_id' => 1,
		]);
                
                $this->insert('store', [
			'name' => 'Fakta',
			'url' => 'http://www.fakta.dk',
			'offers_url' => 'http://www.fakta.dk/tilbudsavis.html',
			'user_id' => 1,
		]);
		
		$this->insert('product', [
			'brand' => 'Nescafe',
			'type' => 'Coffee',
			'desc' => 'Regular',
			'amount' => 200,
			'unit' => 'g',
			'user_id' => 1,
		]);
                
                $this->insert('product', [
			'brand' => 'Nescafe',
			'type' => 'Coffee',
			'desc' => 'Decaf',
			'amount' => 200,
			'unit' => 'g',
			'user_id' => 1,
		]);
		
		$this->insert('product', [
			'brand' => 'Lurpak',
			'type' => 'Butter',
			'desc' => 'Salted',
			'amount' => 250,
			'unit' => 'g',
			'user_id' => 1,
		]);
                
                $this->insert('product', [
			'brand' => 'Castello',
			'type' => 'Cheese',
			'desc' => 'Cheddar, Extra mature',
			'amount' => 350,
			'unit' => 'g',
			'user_id' => 1,
		]);
                
                $this->insert('product', [
			'brand' => 'Dr Oetker',
			'type' => 'Pizza',
			'desc' => 'Ristorante, Pepperoni',
			'amount' => 370,
			'unit' => 'g',
			'user_id' => 1,
		]);
                
                $this->insert('product', [
			'brand' => 'Thise',
			'type' => 'Milk',
			'desc' => 'Jersey',
			'amount' => 1,
			'unit' => 'L',
			'user_id' => 1,
		]);
                
                $this->insert('product', [
			'brand' => 'ecigwizard',
			'type' => 'Battery',
			'desc' => '1100mAh',
			'amount' => 1,
			'user_id' => 1,
		]);

    }

    public function down()
    {
        $this->delete('store', [
			'name' => ['Kvickly','Føtex']
		]);
		
		$this->delete('product', [
			'brand' => 'Nescafe',
			'type' => 'Coffee',
			'desc' => 'Regular',
			'user_id' => 1,
		]);
		$this->delete('product', [
			'brand' => 'Lurpak',
			'type' => 'Butter',
			'desc' => 'Salted',
			'user_id' => 1,
		]);
    }
}
