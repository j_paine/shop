<?php

use yii\db\Schema;
use yii\db\Migration;

class m141101_150241_create_product_table extends Migration
{
    public function up()
    {
		$this->createTable('product', [
			'id' => 'pk',
			'brand' => 'string',
			'type' => 'string',
			'desc' => 'string',
			'amount' => 'integer',
			'unit' => 'string',
			'user_id' => 'integer NOT NULL',
			'created_at' => 'integer NOT NULL',
			'updated_at' => 'integer',
		]);
		
		$this->addForeignKey(
			'fk_product_user', 'product', 'user_id', 'user', 'id', 'CASCADE', 'CASCADE'
		);
    }

    public function down()
    {
        return $this->dropTable('product');
    }
}
