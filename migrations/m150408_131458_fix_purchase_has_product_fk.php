<?php

use yii\db\Schema;
use yii\db\Migration;

class m150408_131458_fix_purchase_has_product_fk extends Migration
{
    public function up()
    {
        $this->dropTable('purchase_has_product');
        $this->createTable('purchase_has_product', [
                'purchase_id' => 'integer NOT NULL',
                'product_id' => 'integer NOT NULL',
                'qty' => 'integer',
                'cost' => 'money NOT NULL',
                'discount' => 'string',
        ]);

        $this->addPrimaryKey(
                'pk_purchase_has_product', 
                'purchase_has_product', 
                ['purchase_id','product_id']
        );

        $this->addForeignKey(
                'fk_purchase_has_product_purchase',
                'purchase_has_product',
                'purchase_id',
                'purchase',
                'id',
                'CASCADE',
                'CASCADE'
        );

        $this->addForeignKey(
                'fk_purchase_has_product_product',
                'purchase_has_product',
                'product_id',
                'product',
                'id',
                'CASCADE',
                'CASCADE'
        );
    }

    public function down()
    {
        $this->dropForeignKey('fk_purchase_has_product_purchase', 'purchase_has_product');
        
        $this->addForeignKey(
                'fk_purchase_has_product_purchase',
                'purchase_has_product',
                'purchase_id',
                'product',
                'id',
                'CASCADE',
                'CASCADE'
        );
    }
}
