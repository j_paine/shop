<?php

use yii\db\Schema;
use yii\db\Migration;

class m141101_150234_create_store_table extends Migration
{
    public function up()
    {
		$this->createTable('store', [
			'id' => 'pk',
			'name' => 'string NOT NULL',
			'url' => 'text',
			'offers_url' => 'text',
			'user_id' => 'integer NOT NULL',
			'created_at' => 'integer NOT NULL',
			'updated_at' => 'integer',
		]);
		
		$this->addForeignKey(
			'fk_store_user', 'store', 'user_id', 'user', 'id', 'CASCADE', 'CASCADE'
		);
    }

    public function down()
    {
        return $this->dropTable('store');
    }
}
