<?php

use yii\db\Schema;
use yii\db\Migration;

class m150316_124612_update_purchase_data extends Migration
{
    public function up()
    {
        $this->delete('purchase', [
            'store_id' => 1,
            'user_id' => 1,
        ]);

        $this->delete('purchase', [
            'store_id' => 2,
            'user_id' => 1,
        ]);
        
        $this->insert('purchase', [
            'date' => strtotime("now"),
            'store_id' => 1,
            'user_id' => 1,
        ]);

        $this->insert('purchase', [
            'date' => strtotime("now"),
            'store_id' => 2,
            'user_id' => 1,
        ]);
    }

    public function down()
    {
        $this->delete('purchase', [
            'store_id' => 1,
            'user_id' => 1,
        ]);

        $this->delete('purchase', [
            'store_id' => 2,
            'user_id' => 1,
        ]);
    }
}
