<?php

use yii\db\Schema;
use yii\db\Migration;

class m141102_174128_add_purchased_items extends Migration
{
    public function up()
    {
		$this->insert('purchase_has_product', [
			'purchase_id' => 1,
			'product_id' => 1,
			'qty' => 1,
			'cost' => 37,
			'discount' => 0,
		]);
		
		$this->insert('purchase_has_product', [
			'purchase_id' => 2,
			'product_id' => 2,
			'qty' => 2,
			'cost' => 10,
			'discount' => '50%',
		]);
    }

    public function down()
    {
        $this->delete('purchase_has_product', [
			'purchase_id' => 1,
			'product_id' => 1,
			'cost' => 37,
			'discount' => 0,
		]);
		
		$this->delete('purchase_has_product', [
			'purchase_id' => 2,
			'product_id' => 2,
			'cost' => 10,
			'discount' => '50%',
		]);
    }
}
