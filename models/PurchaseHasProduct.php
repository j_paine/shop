<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "purchase_has_product".
 *
 * @property integer $purchase_id
 * @property integer $product_id
 * @property integer $qty
 * @property string $cost
 * @property string $discount
 *
 * @property Product $product
 * @property Product $purchase
 */
class PurchaseHasProduct extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'purchase_has_product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cost', 'qty'], 'required'],
            [['cost', 'qty', 'discount'], 'number'],
            ['qty', 'default', 'value' => 1],
            //['discount', 'match', // Allow eg. '£ 32', '3.95 SEK', '3.50', '32%', '12.5 %'
                //'pattern' => '^[^\d]{0,3}[\s]?[\d]{0,6}([\.,][\d]{0,2})?([\s]?[^\d]{0,3})?$|[\d]{0,3}([.,][\d]{1,2})?[\s]?[%]$^'
            //],
            ['discount', 'default', 'value' => 0],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'purchase_id' => Yii::t('app', 'Purchase ID'),
            'product_id' => Yii::t('app', 'Product ID'),
            'qty' => Yii::t('app', 'Quantity'),
            'cost' => Yii::t('app', 'Cost'),
            'discount' => Yii::t('app', 'Discount'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPurchase()
    {
        return $this->hasOne(Product::className(), ['id' => 'purchase_id']);
    }
}
