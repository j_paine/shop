<?php

namespace app\models;

use Yii;
use app\components\behaviors\UserBehavior;
use yii\behaviors\TimestampBehavior;
use app\models\ProductQuery;

/**
 * This is the model class for table "product".
 *
 * @property integer $id
 * @property string $brand
 * @property string $type
 * @property string $desc
 * @property integer $amount
 * @property string $unit
 * @property integer $user_id
 * @property date $created_at
 * @property date $updated_at
 *
 * @property User $user
 * @property PurchaseHasProduct[] $purchaseHasProducts
 */
class Product extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['brand', 'desc'], 'required'],
            ['amount', 'integer'],
            [['brand', 'type', 'desc', 'unit'], 'string', 'max' => 255]
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            UserBehavior::className(),
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'Product'),
            'brand' => Yii::t('app', 'Brand'),
            'type' => Yii::t('app', 'Type'),
            'desc' => Yii::t('app', 'Description'),
            'amount' => Yii::t('app', 'Amount'),
            'unit' => Yii::t('app', 'Unit'),
            'user_id' => Yii::t('app', 'User ID'),
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function find()
    {
        return new ProductQuery(get_called_class());
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPurchaseHasProducts()
    {
        return $this->hasMany(PurchaseHasProduct::className(), ['purchase_id' => 'id']);
    }
}
