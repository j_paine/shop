<?php

namespace app\models;

use yii\db\ActiveQuery;

class ProductQuery extends ActiveQuery 
{
    public function users()
    {
        $this->andWhere(['user_id' => \Yii::$app->user->id]);
        return $this;
    }
}