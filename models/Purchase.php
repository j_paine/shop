<?php

namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;
use dektrium\user\models\User;
use app\components\behaviors\UserBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "purchase".
 *
 * @property integer $id
 * @property string $date
 * @property integer $store_id
 * @property integer $user_id
 *
 * @property User $user
 * @property Store $store
 * @property Products[] $products
 */
class Purchase extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'purchase';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date', 'store_id'], 'required'],
            //['date', 'date', 'format' => 'php:U'],            
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'userBehavior' => UserBehavior::className(),
            'timestampBehavior' => TimestampBehavior::className()
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'date' => Yii::t('app', 'Date'),
            'store_id' => Yii::t('app', 'Store ID'),
            'user_id' => Yii::t('app', 'User ID'),
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStore()
    {
        return $this->hasOne(Store::className(), ['id' => 'store_id']);
    }
	
    public function getPurchasedProducts()
    {
        return $this->hasMany(PurchaseHasProduct::className(), ['purchase_id' => 'id']);
    }
    
    public function getProducts()
    {
        return $this->hasMany(Product::classname(), ['id' => 'product_id'])
                ->via('purchasedProducts'); 
    }

    public function getProductsDataProvider()
    {
        return new ActiveDataProvider([
                'query' => PurchaseHasProduct::find()->where(['purchase_id' => $this->id]),
        ]);
    }
    
    public function getFormAttribs()
    {
        return [
            [
                'attribute' => 'product_id',
                'value' => $this->products->brand,
                'type' => \kartik\builder\TabularForm::INPUT_WIDGET,
                'widgetClass' => \kartik\widgets\Typeahead::className(),
                'options' => [
                    'dataset' => ['abc','def','ghi']
                ]
            ],
            
            
            'qty' => [
                'type' => \kartik\builder\TabularForm::INPUT_WIDGET,
                'widgetClass' => \kartik\widgets\TouchSpin::className(),
            ],
            'cost' => [
                'type' => \kartik\builder\TabularForm::INPUT_WIDGET,
                'widgetClass' => \kartik\widgets\InputWidget::className(),
            ]
            
        ];
    }
}
/*
                    'qty',
                    'product.brand',
                    'product.type',
                    'product.desc',
                    'cost:decimal:Price',
                    'discount',
 * 
 */