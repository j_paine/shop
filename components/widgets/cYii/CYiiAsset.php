<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\components\widgets\cYii;

use yii\web\AssetBundle;

class CYiiAsset extends AssetBundle
{
    public $sourcePath = '@app/components/widgets/cYii/assets';
    
    public function registerScripts()
    {
        $min = YII_DEBUG ? '' : '.min';
        $js = 'js/';
        $css = 'css/';
        
        $this->js[] = $js . 'd3' . $min . '.js';
        $this->js[] = $js . 'c3' . $min . '.js';
        
        $this->css[] = $css . 'c3' . $min . '.css';
        
        return $this;
    }
}