<div id="products"></div>

<script type="text/template" id="products-template">      
    <thead>
        <tr>
            <th>#</th>
            <th>Id</th>
            <th>Product</th>
            <th>Quantity</th>
            <th>Cost</th>
            <th>Discount</th>
            <th></th>
        </tr>
    </thead>
    <tbody></tbody>
</script>

<script type="text/template" id="product-template">
    <td><%= order %></td>
    <td><%= productId %></td>
    <td><%= productName %></td>
    <td><%= qty %></td>
    <td><%= cost %></td>
    <td><%= discount %></td>
    <td>
        <span class="edit glyphicon glyphicon-pencil"></span>
        <span class="delete glyphicon glyphicon-trash"></span>
    </td>
</script>

<script type="text/template" id="no-product-template">
    <td colspan="4">No products</td>
</script>
