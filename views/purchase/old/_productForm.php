<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\web\View;
use kartik\widgets\Typeahead;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $hasProduct app\models\PurchaseHasProduct */
/* @var $product app\models\Product */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-form">
    <?php $productForm = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group field-product-id required">
                <label class="control-label" for="product-id">Product</label>
                <input type="hidden" id="product-id" class="form-control required">
                <?php $suggestionTemplate = '<p>{{brand}}, {{type}}, {{desc}}</p>' .
                               '<p><sm>{{amount}} {{unit}}</sm></p>'; 
                       $footerTemplate = '<p class="text-center"><a href="#">Save this item</a></p>';
                ?>
                    
                <?= Typeahead::widget([
                        'name' => 'product-finder',
                        'pluginOptions' => ['highlight'=>true],
                        'pluginEvents' => [
                            'typeahead:selected' => "function(obj, datum, name) { $('#product-id').val(datum.id); }",
                            
                        ],
                        'dataset' => [
                            [
                                'remote' => Url::to(['product/find']) . '?q=%QUERY',
                                'limit' => 10,
                                'templates' => [
                                    'suggestion' => new JsExpression("Handlebars.compile('${suggestionTemplate}')"),
                                    //'footer' => new JsExpression("Handlebars.compile('${footerTemplate}')"),
                                    'empty' => new JsExpression("Handlebars.compile('${footerTemplate}')")
                                ]
                            ]
                        ]
                ]) ?>
                <div class="help-block"></div>
            </div>
            
        </div>
        <div class="col-sm-6">
            <?= $productForm->field($hasProduct, 'cost')->input('number',['value' => 0, 'onclick' => 'this.select();']) ?>
        </div>
        
    </div>
    <div class="row">
        <div class="col-sm-6">
            <?= $productForm->field($hasProduct, 'qty')->input('number',['value' => 1, 'onclick' => 'this.select();']) ?>
        </div>
        <div class="col-sm-6">
            <?= $productForm->field($hasProduct, 'discount')->input('number',['value' => 0, 'onclick' => 'this.select();']) ?>
        </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton($hasProduct->isNewRecord ? Yii::t('app', 'Add') : Yii::t('app', 'Update'), ['class' => $hasProduct->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php $productForm->end(); ?>
    
</div>

<?php $this->registerJs("$('[name=\"product-finder\"]').on('blur', function() {" .
        "if ($('[name=\"product-finder\"]').val() === '') {" .
            "$('#product-id').val('');" .
        "}" .
        "if ($('#product-id').val() === '') {" .
            "$('.field-product-id .help-block').text('Product cannot be blank.');" .
            "$('.field-product-id').addClass('has-error');" .
            "$('[name=\"product-finder\"]').addClass('has-error');" .
            "$('.field-product-id').removeClass('has-success');" .
        "} else {" .
            "$('.field-product-id .help-block').text('');" .
            "$('.field-product-id').removeClass('has-error');" .
            "$('[name=\"product-finder\"]').removeClass('has-error');" .
            "$('.field-product-id').addClass('has-success');" .
        "}" .
    "});" .
        "$(document).on('focus', 'input', function() { this.select(); });", 
    View::POS_READY);
?>