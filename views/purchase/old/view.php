<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

use dektrium\user\models\User;
use app\components\widgets\cYii\CYii;

/* @var $this yii\web\View */
/* @var $model app\models\Purchase */
/* @var $hasProductModel app\models\PurchaseHasProduct */
/* @var $productModel app\models\Product */

$this->title = $model->store->name . ' - ' . $model->date;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Purchases'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="purchase-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= yii\grid\GridView::widget([
            'dataProvider' => $model->productsDataProvider,
            'columns' => [
                    'qty',
                    'product.brand',
                    'product.type',
                    'product.desc',
                    'cost:decimal:Price',
                    'discount',
            ],
    ]) ?>

</div>
