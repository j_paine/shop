<div class="product-grid">
    
    <div class="products-grid">
        <table class="table table-striped table-bordered" id="products-table">
            <thead>
                <tr>
                    <th>Product</th>
                    <th>Quantity</th>
                    <th>Cost</th>
                    <th>Discount</th>
                    <th></th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
    
    <script id="productRow" type="text/template">
        <td><%= brand %></td>
        <td><%= qty %></td>
        <td><%= cost %></td>
        <td><%= discount %></td>
        <td>
            <a href="#" class="edit" title="Edit"><span class="glyphicon glyphicon-pencil"></span></a>
            <a href="#" class="delete" title="Delete"><span class="glyphicon glyphicon-trash"></span></a>
        </td>
    </script>
    
</div>