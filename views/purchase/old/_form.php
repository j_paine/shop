<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\widgets\ActiveForm;
use kartik\datecontrol\DateControl;
use app\models\Store;

/* @var $this yii\web\View */
/* @var $model app\models\Purchase */
/* @var $hasProduct app\models\PurchaseHasProduct */
/* @var $product app\models\Product */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="purchase-form">

    <?php 
        $form = ActiveForm::begin(); 
        $model->date = date('Y-m-d'); 
    ?>
  
    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($model, 'date')->widget(DateControl::className(),[
                    'type' => DateControl::FORMAT_DATE,
                    'displayFormat' => 'php:d/m/Y',
                    'saveFormat' => 'php:Y-m-d',
                    'readonly' => true,
                    'options' => [
                        'pluginOptions' => [
                            'autoclose' => false,
                            'todayHighlight' => true,
                        ],
                    ],
            ]) ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'store_id')->dropDownList(
                    ArrayHelper::map(Store::find()->all(), 'id', 'name')
            ) ?>
        </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php $form::end(); ?>
    
</div>