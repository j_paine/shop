<?php

use yii\helpers\Html;
use app\assets\PurchaseAppAsset;

/* @var $this yii\web\View */
/* @var $model app\models\Purchase */
/* @var $hasProduct app\models\PurchaseHasProduct */
/* @var $product app\models\Product */

PurchaseAppAsset::register($this);

$this->title = Yii::t('app', 'Create {modelClass}', [
    'modelClass' => 'Purchase',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Purchases'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="purchase-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'hasProduct' => $hasProduct,
        'product' => $product,
    ]) ?>
    
    <?= $this->render('_productForm', [
        'hasProduct' => $hasProduct,
        'product' => $product,
    ]) ?>
    
    <?= $this->render('_grid', [
        
    ]) ?>

    
</div>