<?php

use app\models\Product;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\AutoComplete;
use kartik\widgets\Typeahead;

/* @var $this yii\web\View */
/* @var $hasProductModel app\models\PurchaseHasProduct */
/* @var $productModel app\models\Product */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="purchase-has-product-form">

    <?php $form = ActiveForm::begin(); ?>
  
    <?php echo '<label class="control-label">Product</label>';
        echo Typeahead::widget([
            'name' => 'product-finder',
            'options' => ['placeholder' => 'Find a product ...'],
            'pluginOptions' => ['highlight'=>true],
            'dataset' => [
                [
                    'prefetch' => Url::to(['product/user']),
                    'limit' => 10
                ]
            ]
        ]); ?>

    <?= $form->field($productModel, 'brand')->textInput(['maxlength' => 19]) ?>
    <?= $form->field($productModel, 'type')->textInput(['maxlength' => 19]) ?>
    <?= $form->field($productModel, 'desc')->textInput(['maxlength' => 19]) ?>
    
    <?= $form->field($hasProductModel, 'qty')->textInput(['maxlength' => 19]) ?>
    <?= $form->field($hasProductModel, 'cost')->textInput(['maxlength' => 19]) ?>
    <?= $form->field($hasProductModel, 'discount')->textInput(['maxlength' => 19]) ?>

    <div class="form-group">
        <?= Html::submitButton($hasProductModel->isNewRecord ? Yii::t('app', 'Add') : Yii::t('app', 'Update'), ['class' => $hasProductModel->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
