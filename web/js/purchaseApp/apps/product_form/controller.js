App.module('ProductApp', function(ProductApp, App, Backbone, Marionette, $, _) {
  ProductApp.Controller = {
    showForm: function(id) {
      var product = new App.Entities.Product();//App.request('product:model');
      var products = App.request('product:collection');

      var productFormView = new ProductApp.Form({
        el: $('.product-form'),
        collection: products
      });
      
      productFormView.on('product:create', function(product) {
        console.log('create product', product);
        $.ajax({
          url: '/product/create',
          type: 'POST',
          dataType: 'JSON',
          data: { name: product },
          success: function(id) {
            $('#product-id').val(id);
          }
        });
      });
      
      if (id === undefined)
        this.newProduct(productFormView);
      else
        this.editProduct(products, productFormView, id);
    },
    newProduct: function(view) {
      view.model = App.request('product:model');
      App.productFormRegion.attachView(view);
    },
    editProduct: function(collection, view, id) {
      view.model = collection.get(id);
      console.log('editing', view.model);
      App.productFormRegion.attachView(view);
    }
  };
});
