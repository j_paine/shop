App.module('ProductApp', function(ProductApp, App, Backbone, Marionette, $, _) {
  ProductApp.Form = Marionette.ItemView.extend({
    ui: {
      productId: '#product-id',
      productName: 'input[name="productName"]',
      productHelp: '.field-product-id > .help-block',
      productField: '.field-product-id',
      cost: '#purchasehasproduct-cost',
      discount: '#purchasehasproduct-discount',
      qty: '#purchasehasproduct-qty'
    },
    events: {
      'submit form': 'onFormSubmit',
      'click .save': 'onSaveProduct',
      'blur @ui.productName': 'onProductNameBlur',
      'change @ui.productName': 'onProductNameChange'
    },
    onFormSubmit: function(e) {
      e.preventDefault();
      if (this.model.isNew()) {
        this.collection.create(this.data());
      } else {
        this.collection.set(this.data());
      }
      this.clear();
    },
    onSaveProduct: function(e) {
      e.preventDefault();
      this.trigger('product:create', $(this.ui.productName).val());
    },
    onProductNameBlur: function(e) {
      if ($(this.ui.productName).val() === '') {
        $(this.ui.productId).val('');
      }
      if ($(this.ui.productId).val() === '') {
        $(this.ui.productHelp).text('Select a valid product');
        $(this.ui.productField).addClass('has-error')
          .removeClass('has-success');
      } else {
        $(this.ui.productHelp).text('');
        $(this.ui.productField).removeClass('has-error')
          .addClass('has-success');
      }
    },
    onProductNameChange: function(e) {
      if ($(this.ui.productId).val() !== '')
        $(this.ui.productId).val('');
    },
    data: function() {
      var data = Backbone.Syphon.serialize(this, { exclude: ['_csrf'] });
      data.order = this.collection.nextOrder();
      return data;
    },
    clear: function() {
      var data = {
        productId: '',
        productName: '',
        cost: 0,
        qty: 1,
        discount: 0
      };
      Backbone.Syphon.deserialize(this, data);
      $(this.ui.productName).focus();
    }
  });
});
