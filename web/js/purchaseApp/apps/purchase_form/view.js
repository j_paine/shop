App.module('PurchaseApp', function(PurchaseApp, App, Backbone, Marionette, $, _) {
  PurchaseApp.Form = Marionette.ItemView.extend({
    ui: {
      date: '#purchase-date',
      store: '#purchase-store_id'
    },
    events: {
      'submit form': 'purchaseFormSubmitted'
    },
    purchaseFormSubmitted: function(e) {
      e.preventDefault();
      this.model.set(Backbone.Syphon.serialize(this, { exclude: ['_csrf', 'date-purchase-date'] }).Purchase);
      this.trigger('purchase:create', this.model); 
    }
  });
});
