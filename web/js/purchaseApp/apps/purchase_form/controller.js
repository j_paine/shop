App.module('PurchaseApp', function(PurchaseApp, App, Backbone, Marionette, $, _) {
  PurchaseApp.Controller = {
    showForm: function() {
      var purchase = new App.Entities.Purchase;

      var purchaseFormView = new PurchaseApp.Form({ 
        el: $('.purchase-form'),
        model: purchase 
      });

      purchaseFormView.on('purchase:create', function(model) {
        var products = App.request('product:collection');
        
        var data = {
          purchase: model.toJSON(),
          products: products.toJSON()
        };
        
        $.ajax({
          url: '/purchase/create',
          type: 'POST',
          dataType: 'JSON',
          data: data,
          success: function() {
            products.each(function(model) {
              model.destroy();
            });
          }
        });
      });

      App.purchaseFormRegion.attachView(purchaseFormView);
    }
  };
});
