App.module('ProductApp.Table', function(Table, App, Backbone, Marionette, $, _) {
  Table.Product = Marionette.ItemView.extend({
    tagName: 'tr',
    template: '#product-template',
    triggers: {
      'click .delete': 'product:delete',
      'click .edit': 'product:edit'
    },
    remove: function() {
      var that = this;
      this.$el.fadeOut(function() {
        Marionette.ItemView.prototype.remove.call(that);
      });
    },
  });

  Table.NoProduct = Marionette.ItemView.extend({
    tagName: 'tr',
    template: '#no-product-template'
  });

  Table.Products = Marionette.CompositeView.extend({
    tagName: 'table',
    className: 'table table-striped table-hover',
    template: '#products-template',
    childView: Table.Product,
    childViewContainer: 'tbody',
    emptyView: Table.NoProduct,
    collectionEvents: {
      'change': 'render'
    }
  });
 
});
