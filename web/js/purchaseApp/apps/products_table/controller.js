App.module('ProductApp.Table', function(Table, App, Backbone, Marionette, $, _) {
  Table.Controller = {
    showTable: function() {
      var products = App.request('product:collection');

      var productTableView = new Table.Products({ collection: products });
      
      products.fetch();

      productTableView.on('childview:product:delete', function(attrs) {
        attrs.model.destroy();
      });

      productTableView.on('childview:product:edit', function(attrs) {
        App.trigger('product:edit', attrs.model.get('id'));
      });

      App.productsRegion.show(productTableView);
    }
  };
});
