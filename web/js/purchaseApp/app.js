App = new Marionette.Application();

App.addRegions({
  purchaseFormRegion: '.purchaseForm',
  productFormRegion: '.productForm',
  productsRegion: '#products'
});

App.on('start', function() {
  App.PurchaseApp.Controller.showForm();
  App.ProductApp.Controller.showForm();
  App.ProductApp.Table.Controller.showTable();
});

App.on('product:edit', function(id) {
  console.log('edit event on model', id);
  App.ProductApp.Controller.showForm(id);
});
