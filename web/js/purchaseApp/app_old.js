(function() {
  
  window.App = {
    Models: {},
    Collections: {},
    Views: {}
  };
  
  App.Models.Product = Backbone.Model.extend({
    defaults: {
      productId: 0,
      brand: '',
      type: '',
      desc: '',
      amount: 0,
      unit: '',
      qty: 1,
      cost: 0,
      discount: 0
    }
  });
  
  // Collection of added product models
  App.Collections.Products = Backbone.Collection.extend({
    model: App.Models.Product,
    localStorage: new Backbone.LocalStorage('purchase-app')
  });
  
  // A single product row in the table
  App.Views.Product = Backbone.View.extend({
    tagName: 'tr',
    template: 'productRow',
    events: {
      'click .edit': 'editProduct',
      'click .delete': 'deleteProduct'
    },
    initialize: function() {
      this.model.on('change', this.render, this);
      this.model.on('destroy', this.remove, this);
    },
    render: function() {
      var template = _.template( $('#' + this.template).html() );
      this.$el.html(template(this.model.toJSON()));
      return this;
    },
    editProduct: function (parameters) {
      // 
    },
    deleteProduct: function () {
      this.model.destroy();
    },
    remove: function () {
      this.$el.remove();
    }
  });
  
  // The table of added products
  App.Views.Products = Backbone.View.extend({
    el: '#products-table tbody',
    initialize: function() {
      this.collection.on('add', this.addOne, this);
      this.collection.fetch();
    },
    render: function() {
      this.collection.each(this.addOne, this);
      return this;
    },
    addOne: function(product) {
      var productView = new App.Views.Product({ model: product });
      this.$el.append(productView.render().el);
    }
    
  });
  
  // Form view for adding a product
  App.Views.ProductForm = Backbone.View.extend({
    el: '.product-form form',
    events: {
      'submit': 'submit'
    },
    submit: function(ev) {
      ev.preventDefault();
      this.collection.create({
        productId: $('#product-id').val(),
        brand: $('input[name="product-finder"').val(),
        qty: $('#purchasehasproduct-qty').val(),
        cost: $('#purchasehasproduct-cost').val(),
        discount: $('#purchasehasproduct-discount').val()
      });
      
      $('input[name="product-finder"]').focus();
      console.log(this.collection);
    }
  });
  
})();

$(function() {

  var products = new App.Collections.Products([]);
  
  var productForm = new App.Views.ProductForm({ collection: products });
  
  var productsView = new App.Views.Products({ collection: products });
  
  //productsView.render();
});