App.module('Entities', function(Entities, App, Backbone, Marionette, $, _) {
  Entities.Purchase = Backbone.Model.extend({});

  Entities.Product = Backbone.Model.extend({
    defaults: {
      productId: 0,
      productName: '',
      cost: 0,
      discount: 0,
      qty: 1
    }
  });

  Entities.Products = Backbone.Collection.extend({
    localStorage: new Backbone.LocalStorage('productsApp'),
    model: Entities.Product,
    nextOrder: function() {
      if (!this.length) return 1;
      return this.last().get('order') + 1;
    },
    comparator: function(product) {
      return product.get('order');
    }
  });

  var purchase,
      product,
      products;

  var initPurchase = function() {
    purchase = new Entities.Purchase();
  };

  var initProduct = function() {
    product = new Entities.Product();
  };

  var initProducts = function() {
    products = new Entities.Products();
  };

  var API = {
    getPurchaseModel: function() {
      if (purchase === undefined) initPurchase();
      return purchase; 
    },
    getProductModel: function() {
      if (product === undefined) initProduct();
      return product;
    },
    getProductsCollection: function() {
      if (products === undefined) initProducts();
      return products;
    }
  };

  App.reqres.setHandler('purchase:model', function() {
    return API.getPurchaseModel();
  });

  App.reqres.setHandler('product:model', function() {
    return API.getProductModel();
  });

  App.reqres.setHandler('product:collection', function() {
    return API.getProductsCollection();
  });

}); 
